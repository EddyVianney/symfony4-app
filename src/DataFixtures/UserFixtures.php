<?php

namespace App\DataFixtures;

use App\Entity\User ;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $password_encoder)
    {
        $this->password_encoder  = $password_encoder ;
    }
    public function load(ObjectManager $manager)
    {
        foreach($this->getUserData() as [$name, $last_name,$email,$password,$api_key,$roles])
        {
            $user = new User() ;
            $user->setName($name);
            $user->setLastName($last_name);
            $user->setEmail($email);
            $user->setPassword($this->password_encoder->encodePassword($user, $password));
            $user->setVimeoApiKey($api_key);
            $user->setRoles($roles);
            $manager->persist($user);
        }
        $manager->flush();
    }

    public function getUserData()
    {
        return [

            ['eddddy','pamboudd','novalis@gmail.com','passwww','ggg',['ROLE_ADMIN']],
            ['eddy','pambodddu','valis@gmail.com','passwg',null,['ROLE_ADMIN']],
            ['eddy','pambodddu','nova@gmail.com','passwf','ggg',['ROLE_USER']]
        ];
    }
}
