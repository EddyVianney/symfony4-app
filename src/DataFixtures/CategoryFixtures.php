<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category ;

class CategoryFixtures extends Fixture
{


    public function load(ObjectManager $manager)
    {

        $this->LoadMainCategories($manager);
        $this->LoadElectronics($manager);
        $this->LoadComputers($manager);
        $this->LoadLaptops($manager);
        $this->LoadMovies($manager);
        $this->LoadBooks($manager);
        $this->LoadRomance($manager);
        
    }

    private function LoadMainCategories($manager)
    {

        foreach ($this->getMainCategoriesData() as $name )
        {

            $category = new Category();
            $category->setName($name);
            $manager->persist($category);

        }

        $manager->flush();
    }

    private Function LoadElectronics($manager)
    {
        $this->LoadSubCategories($manager,'Electronics',1);
    }

    private Function LoadComputers($manager)
    {
        $this->LoadSubCategories($manager, 'Computers', 6);
    }

    private Function LoadLaptops($manager)
    {
        $this->LoadSubCategories($manager, 'Laptops', 8);
    }

    private Function LoadBooks($manager)
    {
        $this->LoadSubCategories($manager, 'Books', 3);
    }

    private Function LoadMovies($manager)
    {
        $this->LoadSubCategories($manager, 'Movies', 4);
    }

    private Function LoadRomance($manager)
    {
        $this->LoadSubCategories($manager, 'Romance', 16);
    }
    
    
    private function LoadSubCategories($manager, $category, $parent_id)
    {

        $parent = $manager->getRepository(Category::class)->find($parent_id);
        $method = "get{$category}Data";
        
        foreach ($this->$method($parent_id) as $name )
        {
            $category = new Category();
            $category->setName($name);
            $category->setParent($parent);
            $manager->persist($category);

        }

        $manager->flush();
        
    }

    private function getElectronicsData()
    {
        return ['Cameras','Computers','Cell phones'] ;
    }

    private function getComputersData()
    {
        return ['Laptops','Desktops'];
    }

    private function getLaptopsData()
    {
        return ['Apple','Asus','Dell','Lenevo','HP'];
    }
    
    private function getBooksData()
    {
        return ['Children\'s Books','Kindle eBooks'];
    }

    private function getMoviesData()
    {
        return ['Family','Romance'];
    }

    private function getRomanceData()
    {
        return ['Romantic comedy','Romantic Drama'];
    }


    private function getMainCategoriesData()
    {

        return ['Electronics','Toys','Books','Movies'];
    }

}
