<?php

namespace App\Repository;

use App\Entity\Video;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry,PaginatorInterface $paginator)
    {
        parent::__construct($registry, Video::class);
        $this->paginator = $paginator ;
    }

    public function findByChildIds(array $ids, int $page,?string $sort)
    {

        if($sort != 'rating')
        {
        $dbquery = $this->createQueryBuilder('v')           
                    ->andWhere('v.category IN (:val)')
                    ->leftJoin('v.comments', 'c')
                    ->leftJoin('v.usersThatLike', 'l')
                    ->leftJoin('v.usersThatDontLike', 'd')
                    ->addSelect('c','l','d')
                    ->setParameter('val', $ids)
                    ->orderBy('v.title', $sort);
        }
        else
        {
            $dbquery =  $this->createQueryBuilder('v')
            ->addSelect('COUNT(l) AS HIDDEN likes') // bez hidden zwróci array: count + entity
            ->leftJoin('v.usersThatLike', 'l')
            ->andWhere('v.category IN (:val)')
            ->setParameter('val', $ids)
            ->groupBy('v')
            ->orderBy('likes', 'DESC');
        }

        $dbquery->getQuery();

        $pagination = $this->paginator->paginate($dbquery, $page, 5);
        return $pagination;
    }

    public function findByTitle(string $query, int $page, ?string $sort)
    {
        $querybuilder = $this->createQueryBuilder('v');
        $searchTerms = $this->breakDownQuery($query);

        foreach ($searchTerms as $key => $term)
        {
            $querybuilder
                ->orWhere('v.title LIKE :t_'.$key)
                ->setParameter('t_'.$key, '%'.trim($term).'%'); 
        }

        if($sort != 'rating')
        {
            $dbQuery =  $querybuilder
                ->orderBy('v.title', $sort)
                ->leftJoin('v.comments', 'c')
                ->leftJoin('v.usersThatLike', 'l')
                ->leftJoin('v.usersThatDontLike', 'd')
                ->addSelect('c','l','d')
                ->getQuery();
        }
        else
        {
            $dbQuery =  $querybuilder
            ->addSelect('COUNT(l) AS HIDDEN likes') // bez hidden zwróci array: count + entity
            ->leftJoin('v.usersThatLike', 'l')
            ->groupBy('v')
            ->orderBy('likes', 'DESC')
             ->getQuery();
        }
        
        
        return $this->paginator->paginate($dbQuery,$page,5);
    }

    public function breakDownQuery(string $query):array
    {
        /* use  the function array_unique to ensure that there will be no repeated string in the result */
        $sterms = array_unique(explode(" ", $query));
        /*the search will be performed only when a string has at least two letters */

        return array_filter($sterms,function($term){

            return mb_strlen($term) >= 2 ;
        });
    }

    public function videoDetails($id)
    {

        return $this->createQueryBuilder('v')
        ->leftjoin('v.comments','c')
        ->leftjoin('c.user','u')
        ->addSelect('c','u')
        ->where('v.id = :id')
        ->setParameter('id',$id)
        ->getQuery()
        ->getOneOrNullResult();

    }

  
}
