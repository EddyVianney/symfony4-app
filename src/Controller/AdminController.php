<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\CategoryTreeAdminList ;
use App\Entity\Category ;
use App\Utils\CategoryTreeAdminOptionList ;
use App\Form\CategoryType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Video;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     */
    public function index()
    {
        return $this->render('admin/my_profile.html.twig');
    }


     /**
     * @Route("/su/categories", name="categories", methods={"POST","GET"})
     */
    public function categories(CategoryTreeAdminList $categories, Request $request)
    {
        $categories = $categories->getCategoryList($categories->buildTree());

        $category = new Category();
        $form = $this->createForm(CategoryType::class,$category);
        $is_invalid = null ;
        if($this->saveCategory($category, $form, $request))
        {

            return $this->redirectToRoute('categories');
        }
        elseif($request->isMethod('post'))
        {
            $is_invalid = 'is-invalid';
        }

        return $this->render('admin/categories.html.twig',['categories'=> $categories,
        'form'=>$form->createView(), 'is_invalid'=>$is_invalid]);

        return $this->redirectToRoute('categories');
    }

     /**
     * @Route("/videos", name="videos")
     */
    public function videos()
    {
        $videos = null ;

        if($this->isGranted('ROLE_ADMIN'))
        {
            $videos = $this->getDoctrine()
            ->getRepository(Video::class)
            ->findAll();
        }
        else
        {
            $videos = $this->getUser()->getLikedVideos() ;
        }
        return $this->render('admin/videos.html.twig',['videos'=>$videos]);
    }

     /**
     * @Route("/su/upload-video", name="upload_video")
     */
    public function uploadVideo()
    {
        return $this->render('admin/upload_video.html.twig');
    }


     /**
     * @Route("/users", name="users")
     */
    public function users()
    {
        return $this->render('admin/users.html.twig');
    }


     /**
     * @Route("/su/edit-category/{id}", name="edit_category",methods={"POST","GET"})
     */
    public function editCategory(Category $category,Request $request)
    {
        $form = $this->createForm(CategoryType::class,$category);
        $is_invalid = null ;

        if($this->saveCategory($category, $form, $request))
        {

            return $this->redirectToRoute('categories');
        }
        elseif($request->isMethod('post'))
        {
            $is_invalid = 'is-invalid';
        }

        return $this->render('admin/edit_category.html.twig',['category'=>$category,
        'form'=>$form->createView(),'is_invalid'=>$is_invalid]);
    }

    /**
     * @Route("/su/delete-category/{id}", name="delete_category")
     */
    public function deleteCategory(Category $category)
    {
        $entityManager =$this->getDoctrine()->getManager();
        $entityManager->remove($category);
        $entityManager->flush();
        return $this->redirectToRoute('categories');
    }

    public function getAllCategories(CategoryTreeAdminOptionList $categories,$editedCategory = null)
    {
        $categories->getCategoryList($categories->buildTree());
        return $this->render('admin/_all_categories.html.twig',['categories'=>$categories, 'editedCategory'=>$editedCategory]);

    }


    private function saveCategory($category, $form ,$request)
    {
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $category->setName($request->request->get('category')['name']);
            $repository = $this->getDoctrine()->getRepository(Category::class);
            $parent = $repository->find($request->request->get('category')['parent']);
            $category->setParent($parent);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            return true ;
        }

        return false;

    }

    

}
