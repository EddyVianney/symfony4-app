<?php


namespace App\Utils;
use App\Utils\AbstractClasses\CategoryTreeAbstract;
use App\Twig\AppExtension ;


class CategoryTreeFrontPage extends CategoryTreeAbstract {

public function getCategoryList(array $categories)
{

    $this->categoryList .= '<ul>';

    foreach($categories as $value )
    {
        $categoryName = $this->slugger->slugify($value['name']);
        $url = $this->urlGenerator->generate('video_list',['categoryname'=> $categoryName ,'id'=>$value['id'] ]);
        $this->categoryList .= '<li>'.'<a href=" '.$url.'">'.$value['name'].'</a> </li>';

        if(!empty($value['children']))
        {
            $this->getCategoryList($value['children']);
        }

    }
    $this->categoryList .= '</ul>';

    return $this->categoryList ;
}

public function getMainParent(int $id): array
{

    $key = array_search($id,array_column($this->categories,'id'));

    if( $this->categories[$key]['parent_id'] != null)
    {
        return $this->getMainParent($this->categories[$key]['parent_id']);
    }
    else
    {
        return ['id'=>$this->categories[$key]['id'],
        'name'=>$this->categories[$key]['name'] ];
    }
}

public function getCategoryListAndParent(int $id): string
{
    $this->slugger = new AppExtension ;
    $parentData = $this->getMainParent($id);
    $this->mainParentName = $parentData['name'];
    $this->mainParentId =  $parentData['id'];
    $key = array_search($id, array_column($this->categories,'id'));
    $this->currentCategoryName = $this->categories[$key]['name'];
    $category_array = $this->buildTree($parentData['id']);

    return $this->getCategoryList($category_array);
}

public function getChildIds(int $parent): array
{
    static $childIds= []; // for avoiding that the array is empty after at new recursion

    foreach($this->categories as $category)
    {

        if($category['parent_id'] == $parent )
        {
            $childIds[] = $category['id'].',';
            $this->getChildIds($category['id']);
        }

    }

    return $childIds;
}


}