#!/bin/bash

if [ "$2" == "-db" ]

then

echo "resetting database ..."

sudo bin/console doctrine:schema:drop -n -q --force --full-database &&
sudo rm src/Migrations/*.php &&
sudo bin/console make:migration &&
sudo bin/console doctrine:migrations:migrate -n -q &&
sudo bin/console doctrine:fixture:load -n -q

fi 

if [ -n "$1" ]

then 

#lauching tests on  a specific folder
./bin/phpunit $1

else

 ##lauching  all tests
./bin/phpunit


fi 