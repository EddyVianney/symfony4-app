<?php

namespace App\Tests\Twig;

use PHPUnit\Framework\TestCase;
use App\Twig\AppExtension ;

class SluggerTest extends TestCase
{

    /**
     * @dataProvider getSlugs
   */
    public function testSlugify($inputString, $outputString)
    {
        $slugger = new AppExtension ;
        $this->assertSame($outputString,$slugger->slugify($inputString));
    }


    public function getSlugs()
    {
        return [

            ['Books children','books-children'],
            ['Three games QI','three-games-qi'],
            ['Books children','books-children'],
            ['BooKs chiLdren','books-children'],
            ['  BooKs chiLdren','books-children'],
            ['?BooKs ?chiLdren','books-children'],
            ['PleasUre betweeN 60 and 70','pleasure-between-60-and-70']
        ];
    }
}
