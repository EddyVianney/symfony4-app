<?php

namespace App\Tests\Utils;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Twig\AppExtension ;

class CategoryTest extends KernelTestCase
{
    protected $mockedCategoryTreeFrontPage ;
    protected $mockedCategoryTreeAdminPage ;
    protected $mockedCategoryTreeAdminOptionList;

    protected function setUp()
    {
        $kernel = self::bootKernel();
        $urlGenerator = $kernel->getContainer()->get('router');

        $testedClasses = ['CategoryTreeFrontPage','CategoryTreeAdminPage','CategoryTreeAdminOptionList'];

        foreach($testedClasses as $class)
        {

            $pathClass = 'App\Utils\\'.$class;
            $mockedClass = 'mocked'.$class;
            $this->$mockedClass = $this->getMockBuilder($pathClass)
            ->disableOriginalConstructor()
            ->setMethods() // keep methods from the original class
            ->getMock();
            $this->$mockedClass->urlGenerator = $urlGenerator ;
        }

    }
    
    /**
     * @dataProvider dataForCategoryTreeFrontPage
     */
    public function testCategoryTreeFrontPage($string, $array,$id)
    {
        $this->mockedCategoryTreeAdminOptionList->categories = $array;
        $this->mockedCategoryTreeFrontPage->categories = $array;
        $this->mockedCategoryTreeFrontPage->slugger = new AppExtension ;
        $mainParentid = $this->mockedCategoryTreeFrontPage->getMainParent($id)['id'];
        $array = $this->mockedCategoryTreeFrontPage->buildTree($mainParentId);
        $this->assertSame($tring,$this->mockedCategoryTreeFrontPage->getCategoryList($array) );
    }

    /**
     * @dataProvider dataForCategoryTreeAdminOptionList
     */
    public function testCategoryTreeAdminOptionList($arrayToCompare, $arrayFromDb)
    {
        $this->mockedCategoryTreeAdminOptionList->categories = $arrayFromDb;
        $arrayFromDb = $this->mockedCategoryTreeAdminOptionList->buildTree();
        $this->assertSame($arrayToCompare, $this->mockedCategoryTreeAdminOptionList->getCategoryList($arrayFromDb));
    }


    /**
     * @provider dataFortestmockedCategoryTreeAdminPage
     */
    public function testmockedCategoryTreeAdminPage()
    {
        // $this->mockedCategoryTreeAdminPage->categories = $array;
        // $array = $this->mockedCategoryTreeAdminPage->buildTree();
        // $this->assertSame($string, $this->mockedCategoryTreeAdminPage->getCategoryList($array));

    }

    public function dataForCategoryTreeAdminOptionList()
    {
        yield [
            [
                ['name'=>'Electronics','id'=>1],
                ['name'=>'--Computers','id'=>6],
                ['name'=>'----Laptops','id'=>8],
                ['name'=>'------HP','id'=>14]
            ],
            [ 
                ['name'=>'Electronics','id'=>1, 'parent_id'=>null],
                ['name'=>'Computers','id'=>6, 'parent_id'=>1],
                ['name'=>'Laptops','id'=>8, 'parent_id'=>6],
                ['name'=>'HP','id'=>14, 'parent_id'=>8]
            ]
         ];
    }

    public function dataForCategoryTreeFrontPage()
    {
        return  [];
    }

    public function dataFortestmockedCategoryTreeAdminPage()
    {
        return  [];
    }
}
