<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FrontControllerLikesTest extends WebTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->client = static::createClient([], [
            'PHP_AUTH_USER' => 'nova@gmail.com',
            'PHP_AUTH_PW' => 'passwf',
        ]);
        $this->client->disableReboot();
  
        $this->entityManager = $this->client->getContainer()->get('doctrine.orm.entity_manager');
        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->entityManager->rollback();    
        $this->entityManager->close();    
        $this->entityManager = null; // avoid memory leaks   
    }

    public function testLike()
    {
        //$client = static::createClient();
        $this->client->request('POST', '/video-list/6/like');
        $crawler = $this->client->request('GET', '/video-list/category/movies/4/2');

        $this->assertSame('(1)', $crawler->filter('small.number-of-likes-6')->text() );
    }

    public function testDisLike()
    {
        //$client = static::createClient();
        $this->client->request('POST', '/video-list/6/dislike');
        $crawler = $this->client->request('GET', '/video-list/category/movies/4/2');

        $this->assertSame('(1)', $crawler->filter('small.number-of-dislikes-6')->text() );
    }

    public function testNumhberOfLikeVideos()
    {
        //$client = static::createClient();

        /* one cannot like a video already liked */
        $this->client->request('POST', '/video-list/1/like');
        $this->client->request('POST', '/video-list/1/like');

        $crawler = $this->client->request('GET', '/admin/videos');

        $this->assertEquals(4, $crawler->filter('td > a ')->count() );
    }

    public function testNumhberOfLikeVideos2()
    {
        //$client = static::createClient();

        /* two unlike request */
        $this->client->request('POST', '/video-list/1/unlike');
        $this->client->request('POST', '/video-list/2/unlike');

        $crawler = $this->client->request('GET', '/admin/videos');

        $this->assertEquals(2, $crawler->filter('td > a ')->count());
    }


}
