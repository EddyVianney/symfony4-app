<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FrontControllerVideoTest extends WebTestCase
{
    public function testNoResults()
    {
        $testString = "www";

        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('Search video')->form(['query'=>$testString]);
        $crawler = $client->submit($form);
        $this->assertContains('No results were found',$crawler->filter('h1')->text());

    }

    /* do not create a function name starting by a A-Z * : the tes will not be executed */
    public function testResultsFound()
    {

        $client = static::createClient();
        $client->followRedirects();
        
        $crawler = $client->request('GET', '/');
    
        $form = $crawler->selectButton('Search video')->form([
            'query' => 'Movies',
        ]);
        $crawler = $client->submit($form);

        $this->assertGreaterThan(4, $crawler->filter('h3')->count());

    }

    public function ts(){

    }

    public function testSorting()
    {

        $client = static::createClient();
        $client->followRedirects();
        
        $crawler = $client->request('GET', '/');
        
        $form = $crawler->selectButton('Search video')->form([
            'query' => 'Movies',
        ]);
        $crawler = $client->submit($form);

        $form = $crawler->filter('#form-sorting')->form([
            'sortby'=>'desc',
        ]);

        $crawler = $client->submit($form);

        for($i = 9 ; $i > 4; $i --)
        {
            static $domPos = 0 ; // position de la video dans la DOM
            $this->assertEquals('Movies '.$i, $crawler->filter('h3')->eq($domPos)->text());
            $domPos ++ ;   
        }

        $this->assertEquals('Video title', $crawler->filter('h3')->last()->text());
        $this->assertGreaterThanOrEqual(6, $crawler->filter('div.card-body')->count());
        

    }
}
