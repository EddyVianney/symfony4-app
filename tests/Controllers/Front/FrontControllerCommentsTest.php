<?php

namespace App\Tests;
/*use App\Tests\Rollback; */

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FrontControllerCommentsTest extends WebTestCase
{
  /*  use Rollback; */

  public function setUp()
  {
      parent::setUp();
      $this->client = static::createClient([], [
          'PHP_AUTH_USER' => 'novalis@gmail.com',
          'PHP_AUTH_PW' => 'passwww',
      ]);
      $this->client->disableReboot();

      $this->entityManager = $this->client->getContainer()->get('doctrine.orm.entity_manager');
      $this->entityManager->beginTransaction();
      $this->entityManager->getConnection()->setAutoCommit(false);
  }


    public function testNotLoggedInUser()
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $this->client->request('GET', '/video-details/16');

        $form = $crawler->selectButton('Add')->form([
            'comment'=>'Test comment'
        ]);

        $client->submit($form);

        $this->assertContains('Please sign in',$client->getResponse()->getContent());
    }

    public function testNewCommentAndNumberOfComments()
    {
 
        $this->client->followRedirects();

        $crawler = $this->client->request('GET', '/video-details/1');

        $form = $crawler->selectButton('Add')->form([
            'comment' => 'Test comment1',
        ]);
        $this->client->submit($form);

        $this->assertContains('Test comment1', $this->client->getResponse()->getContent() );

        /*$crawler = $this->client->request('GET', '/video-list/category/toys,2');
        $this->assertSame('Comments (1)', $crawler->filter('a.ml-1')->text());*/
        
    }
    
    public function tearDown()
    {
        parent::tearDown();
        $this->entityManager->rollback();    
        $this->entityManager->close();    
        $this->entityManager = null; // avoid memory leaks   
    }
  


}


