<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Category;
/*use App\Tests\Rollback; */

class AdminControllerCategoriesTest extends WebTestCase
{
    
    /*use Rollback;*/

    public function setUp()
    {
        parent::setUp();
        $this->client = static::createClient([], [
            'PHP_AUTH_USER' => 'novalis@gmail.com',
            'PHP_AUTH_PW' => 'passwww',
        ]);
        $this->client->disableReboot();

        $this->entityManager = $this->client->getContainer()->get('doctrine.orm.entity_manager');
        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->entityManager->rollback();    
        $this->entityManager->close();    
        $this->entityManager = null; // avoid memory leaks   
    }
    
    public function testTextOnPage()
    {
        $crawler = $this->client->request('GET', 'admin/su/categories');
        $this->assertSame('Categories list',$crawler->filter('h2')->text());
        $this->assertContains('Electronics', $this->client->getResponse()->getContent());
        //  $this->assertResponseIsSuccessful();
        //  $this->assertSelectorTextContains('h1', 'Hello World');
    }
    public function numberOfItems()
    {
        $crawler = $this->client->request('GET', 'admin/su/categories');
        $this->assertCount(20,$crawler->filter('option'));


    }

    /* testing the addition of a new category */
    public function testNewCategory()
    {
        $parentId = 1;
        $categoryName = 'Others electronics';

        /* fetching web page */
        $crawler = $this->client->request('GET', 'admin/su/categories');

        $form = $crawler->selectButton('Add')->form([
        'category[parent]'=> $parentId,
        'category[name]'=> $categoryName,
        ]);
        /* sending form */     
        $this->client->submit($form);
        
        /* fetching the new added category in the database */
        $category = $this->entityManager->getRepository(Category::class)->findOneBy(['name'=>'Others electronics']);
        
        /*  */
        $this->assertNotNull($category);
        $this->assertSame('Others electronics',$category->getName());
    }

    public function testEditCategory()
    {
        $parentId = 0;
        $categoryNewName = 'Others electronics 2';

        $crawler = $this->client->request('GET', 'admin/su/edit-category/1');
        $form = $crawler->selectButton('Save')->form([
            'category[parent]'=> $parentId,
            'category[name]'=> $categoryNewName,
            ]);
        $this->client->submit($form);
        $category = $this->entityManager->getRepository(Category::class)->find(1);
        $this->assertSame('Others electronics 2',$category->getName());
        
    }

    public function  testDeleteCategory()
    {
        $id = 2 ;
        $crawler = $this->client->request('GET', 'admin/su/delete-category/'.$id);
        $category = $this->entityManager->getRepository(Category::class)->find($id);
        $this->assertNull($category);
    }
}
